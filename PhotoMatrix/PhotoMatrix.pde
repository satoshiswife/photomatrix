/*
// Nicole Vella
//
// Please accredit any use.
//
*/

import com.hamoid.*;
VideoExport videoExport;

// ints for width, height, and skip/resolution 
int w, h, skip;

// if you want a border on your image
int border = 0;

// floats for scale, the newly mapped depth of the vector, radius and angle for rotating/animating
float scl, newDepth, rad, a = 0;

// the image stuff
PImage img;
String imgname = "example";

void settings() {
  // turn smoothing on
  smooth();

  //load our image into a pixel array
  img = loadImage(imgname+".png");
  img.loadPixels();

  // set the canvas the same size as the image, and make sure its an even number
  w=img.width+border;
  h=img.height+border;
  if (w % 2 == 1) { 
    w++;
  } 
  if (h % 2 == 1) { 
    h++;
  } 
  size(w, h, P3D);
}

void setup() {
  // video export setup
  videoExport = new VideoExport(this, imgname+"_"+day()+hour()+minute()+second()+".mp4");
  videoExport.startMovie();

  // init variables
  newDepth = 0.;
  skip = 4; // control the resultion with skip; higher skip = lower res
  scl = 1; // you can scale up the point cloud if you wish
}

void draw() {
  background(255);

  // move to the center
  translate(w/2, h/2);

  // if it's rotated twice, save the animation
  if (a >720) {
    save(imgname+"_"+day()+hour()+minute()+second());
    videoExport.endMovie();
    noLoop();
    if (a==180 || a==360 || a==540) {
      // save images at various angles
      save(imgname+"_"+day()+hour()+minute()+second());
    }
  } else {

    rad = radians(a);
    videoExport.saveFrame();
    rotateX(rad);
    // rotateY(rad);
    // rotateZ(rad);

    println(a);
    a += 0.5;
  }

  // amplitude as brightness
  // iterate through the pixel array
  for (int x = 0; x < img.width; x += skip) {
    for (int y = 0; y < img.height; y += skip) {

      // save draw settings
      pushMatrix();

      // get the colour from current pixel
      color c = img.get(x, y);

      // set vector based on pixel brightness
      PVector pos = new PVector(x+(border/2), y+(border/2));
      pos.z = brightness(c);

      // colour the vector
      stroke(c);

      // move into 3D position
      translate(-(w/2), -(h/2), 0);
      newDepth = map(pos.z, 0, 255, -w/4, w/4);
      if (c < 255 || c > 0) {
        // ignore the pure white and black pixels
        point(pos.x*scl, pos.y*scl, pos.z*scl);
      }

      // revert draw settings
      popMatrix();
    }
  }
}